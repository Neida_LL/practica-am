import { Injectable } from '@angular/core';
import { NodoThree } from '../interface/datos.interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ThreeService {

  constructor(public http: HttpClient) { }
  getInfo(): Observable<NodoThree[]>{
    return this.http.get<NodoThree[]>('./assets/data.json')
  }
}
