import { Component, OnInit } from '@angular/core';

//interface
import { NodoThree } from 'src/app/interface/datos.interface';
//requeridos
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThreeService } from 'src/app/services/three.service';


@Component({
  selector: 'app-formthree',
  templateUrl: './formthree.component.html',
  styleUrls: ['./formthree.component.css']
})
export class FormthreeComponent implements OnInit {


  dataSource = new MatTreeNestedDataSource<NodoThree>();
  controlT = new NestedTreeControl<NodoThree>(nodo => nodo.children);
  form!:FormGroup;
  valn!:number;
  n!:number;
  val:string='';
  constructor(private fb:FormBuilder,private threeS: ThreeService ) { 
    this.generarForm()
  }
  ngOnInit(): void {
  }


  generarForm(){
    this.form = this.fb.group({
      niveles: [1, [Validators.min(1), Validators.max(100), Validators.required]],
      repeticiones: [1, [Validators.min(1), Validators.max(100), Validators.required]],
    valor1: ['', [Validators.required]],
    valor2: ['', [Validators.required]],
    valor3: ['', [Validators.required]],
    valor4: ['', [Validators.required]]
    })
  }

  hasChild = (_: number, node: NodoThree) => !!node.children && node.children.length > 0;

  guardar(): void{
    console.log('guardar');
    
  console.log(this.form.value);
  this.valn = this.form.value.Nvel
  
 this.val = this.form.value.valor1

  }

  concatenacionNodos(res:NodoThree[], rep:number, value:string): NodoThree[] {
    var listHelp: NodoThree[] = []

    for (let i = 0; i < rep; i++) {
      let pattern: NodoThree={
        name: value,
        children: res
      }
      listHelp.push(pattern)
    }
    return listHelp
  }


  getVal(num: number, values: string[]): string {
    if (num > 4) {
      return this.getVal(num-4, values)
    } else {
      return values[num-1]
    }
  }

  generarD() {
    let {niveles, repeticiones, valor1, valor2, valor3, valor4} = this.form.value
    var values =[valor1, valor2, valor3, valor4]
    var result:NodoThree [] = []
    var primeraVez = true

    

    while (niveles > 0) {
      let value = this.getVal(niveles,values)

        result = this.concatenacionNodos(result, repeticiones, value)
      
      niveles--
    }
   
    this.dataSource.data = result;
  }

  add() {
    this.form.valid? this.generarD(): ""
  }
}
