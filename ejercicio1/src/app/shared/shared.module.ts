import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {MatSelectModule} from '@angular/material/select'; 
import {MatSnackBarModule} from '@angular/material/snack-bar'; 
import {MatInputModule} from '@angular/material/input'; 
import {MatButtonModule} from '@angular/material/button'; 
import {MatTreeModule} from '@angular/material/tree'; 
import {MatIconModule} from '@angular/material/icon'; 


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSelectModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
    MatTreeModule,
    MatIconModule,
    
  ],
  exports:[
    CommonModule,
    MatSelectModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
    MatTreeModule,
    MatIconModule
  ]

})
export class SharedModule { }
