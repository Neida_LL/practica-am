import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formularioreact',
  templateUrl: './formularioreact.component.html',
  styleUrls: ['./formularioreact.component.css']
})
export class FormularioreactComponent implements OnInit {

  guardados:string[]=[];
  formulario!:FormGroup;
  constructor(private fb:FormBuilder) { 
    this.crearForm();
  }

 
  get getInputs(){
    return this.formulario.get('input1') as FormArray;
  }


  ngOnInit(): void {
  }

  crearForm(){
    this.formulario = this.fb.group({
      input1:this.fb.array([]), 
    })

    this.getInputs.push(
      this.fb.group({
      textoNuevo : [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/),Validators.required]]
    }))
  }

  save():void{
  
    for (let i = 0; i < this.getInputs.length; i++) {
      
    if (this.getInputs.at(i).get('textoNuevo')?.valid) {
      if(this.getInputs.at(i).get('textoNuevo')?.value !== null && (this.getInputs.at(i).get('textoNuevo')?.value != '')){
        this.guardados.push(this.getInputs.at(i).get('textoNuevo')?.value);
     }
    }
    }
  
  }

  limpiar(id:number){
    for (let i = 0; i < this.getInputs.length; i++) {
      if (id===i) {
        this.getInputs.at(i).get('textoNuevo')?.setValue('');
      }
    }
  }

  deleteInput(id:number):void{
    this.getInputs.removeAt(id)
  }

  clearMulti():void{
    this.guardados = [];
    this.formulario.reset;
  }

  clearInputReact():void{
    this.guardados=[];
  }

  add():void{
    const nuevo = this.fb.group({
      textoNuevo : [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/), Validators.required]]
    })
    this.getInputs.push(nuevo);
  }

  toShow():string{
    return this.guardados.join("\n");
 }



}
